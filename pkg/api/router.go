package api

import (
	"iseng/pkg/database"
	"iseng/pkg/user"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// func RegisterRouter(database *database.Database) *gin.Engine {
// 	r := gin.Default()
// 	return r
// }

func SetupRouter(db *database.Database) (*gin.Engine, error) {
	r := gin.New()
	r.Use(gin.Recovery()) // to recover gin automatically
	r.Use(jsonLoggerMiddleware())
	r.Use(prometheusMiddleware())
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	r.GET("/ping", Ping)
	r.GET("/metrics", gin.WrapH(promhttp.Handler()))
	r.POST("/register", user.CreateUser)
	r.POST("/login", user.LoginUser)

	// r.Use(func(c *gin.Context) {
	// 	start := time.Now()
	// 	c.Next()
	// 	latency := time.Since(start)

	// 	logrus.WithFields(logrus.Fields{
	// 		"status":   c.Writer.Status(),
	// 		"method":   c.Request.Method,
	// 		"path":     c.Request.URL.Path,
	// 		"latency":  latency,
	// 		"clientIP": c.ClientIP(),
	// 	}).Info("Request processed")
	// })
	return r, nil
}
