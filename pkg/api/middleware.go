package api

import (
	"encoding/json"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total number of HTTP requests",
		},
		[]string{"method", "endpoint"},
	)
)

func jsonLoggerMiddleware() gin.HandlerFunc {
	return gin.LoggerWithFormatter(
		func(params gin.LogFormatterParams) string {
			log := make(map[string]interface{})

			log["status_code"] = params.StatusCode
			log["path"] = params.Path
			log["method"] = params.Method
			log["start_time"] = params.TimeStamp.Format("2006/01/02 - 15:04:05")
			log["remote_addr"] = params.ClientIP
			log["response_time"] = params.Latency.String()

			s, _ := json.Marshal(log)
			return string(s) + "\n"
		},
	)
}

func prometheusMiddleware() gin.HandlerFunc {
	// Create a new HistogramVec for tracking the request duration
	durationHistogram := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "http_request_duration_seconds",
		Help:    "Histogram of request duration in seconds",
		Buckets: prometheus.DefBuckets,
	}, []string{"method", "endpoint"})

	// Register the HistogramVec with Prometheus
	prometheus.MustRegister(requestCounter)
	prometheus.MustRegister(durationHistogram)

	return func(c *gin.Context) {
		start := time.Now()

		c.Next()

		// Record the request duration
		duration := time.Since(start).Seconds()
		durationHistogram.WithLabelValues(c.Request.Method, c.Request.URL.Path).Observe(duration)
		requestCounter.WithLabelValues(c.Request.Method, c.Request.URL.Path).Inc()
	}
}
