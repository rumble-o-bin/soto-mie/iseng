package user

import (
	"iseng/pkg/database"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateUser(c *gin.Context) {
	var userJSON database.User

	db := c.MustGet("db").(*database.Database)

	if err := c.ShouldBindJSON(&userJSON); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := database.Save(db, &userJSON)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	c.JSON(http.StatusOK, gin.H{"user": userJSON.Username})
}

func LoginUser(c *gin.Context) {
	var userJSON database.User

	db := c.MustGet("db").(*database.Database)

	if err := c.ShouldBindJSON(&userJSON); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	user, err := database.Get(db, userJSON.Username)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	if user.Password != userJSON.Password {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Incorrect username or password"})
	}
	c.JSON(http.StatusOK, gin.H{"user logged": userJSON.Username})
}
