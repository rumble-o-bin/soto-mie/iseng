package database

type User struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func Save(d *Database, user *User) error {
	pipe := d.Client.TxPipeline()
	pipe.HSet(Ctx, user.Username, "password", user.Password)
	_, err := pipe.Exec(Ctx)
	if err != nil {
		return err
	}
	return nil
}

func Get(d *Database, username string) (*User, error) {
	pipe := d.Client.TxPipeline()
	password := pipe.HGet(Ctx, username, "password")
	_, err := pipe.Exec(Ctx)
	if err != nil {
		return nil, err
	}
	if password == nil {
		return nil, ErrNil
	}
	return &User{
		Username: username,
		Password: password.Val(),
	}, nil
}
