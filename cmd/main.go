// Hall of fame:
// - https://gitlab.com/idoko/rediboard
// - https://github.com/mcaci/login
// - https://github.com/Faqihyugos/rest-api-crud-gin

package main

import (
	"iseng/pkg/api"
	"iseng/pkg/config"
	"iseng/pkg/database"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {
	// logrus.SetFormatter(&logrus.TextFormatter{})
	// logrus.SetLevel(logrus.DebugLevel)
	config, err := config.LoadConfig("./config")
	if err != nil {
		log.Fatal("can't read", err)
	}

	redis, err := database.InitDatabase(config.Redis.Address)
	if err != nil {
		log.Fatal("can't connect Redis", err)
	}

	ginMode := gin.ReleaseMode
	switch config.Server.Mode {
	case "debug":
		ginMode = gin.DebugMode
	case "test":
		ginMode = gin.TestMode
	case "release":
		ginMode = gin.ReleaseMode
	default:
		log.Fatalf("invalid Gin mode specified in config.yaml: %s", config.Server.Mode)
	}

	gin.SetMode(ginMode)

	r, err := api.SetupRouter(redis)
	if err != nil {
		log.Fatal("failed to set up router", err)
	}

	err = r.Run(config.Server.Port)
	if err != nil {
		log.Fatal("server can't run", err)
	}
}
