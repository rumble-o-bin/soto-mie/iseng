# First build --> https://gitlab.com/rumble-o-bin/dockerfile/pocketbase/-/blob/main/Dockerfile
ARG ALPINE_VERSION=3.17
ARG GOLANG_VERSION=1.18.1
FROM golang:${GOLANG_VERSION}-alpine as builder
RUN apk --no-cache add git make
WORKDIR /iseng
ADD . /iseng
RUN go mod download \
    && make build

FROM alpine:${ALPINE_VERSION} as runtime
RUN apk update \
    && apk add ca-certificates git tzdata tini \
    && rm -rf /var/cache/apk/*
ENV TZ=Asia/Jakarta
WORKDIR /cmd/iseng
COPY --from=builder /iseng/dist/iseng .

ENTRYPOINT ["/sbin/tini", "--"]
CMD [ "./iseng" ]