# Some cool notes: https://github.com/riskiwah/nothingtodo/blob/main/Makefile
# throw your variables here

.DEFAULT_GOAL := help
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

hello: ## hello
	@echo "hello!"

build: ## build binary
	@CGO_ENABLED=0 go build -gcflags=all="-l -B -C" -ldflags="-s -w -v" -o dist/iseng cmd/main.go

build-container: ## build container
	@docker build -t iseng .

run-container: ## run local container
	@docker run --rm -p 8080:8080 -v $(CURDIR)/config/config.yaml:/cmd/iseng/config/config.yaml iseng:latest

run-dev: ## running dev mode
	@go run cmd/main.go

lint-go: ## Linter
	@golangci-lint run -v

