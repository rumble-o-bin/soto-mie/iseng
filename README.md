# Iseng

<!--toc-->
- [Iseng](#iseng)
    * [Prerequisite](#prerequisite)
    * [Running/Usage](#runningusage)
        * [Running Redis Locally](#running-redis-locally)

<!-- tocstop -->

A nothing todo ways CRUD playground

## Prerequisite

- go v1.18++
- golangci-lint (*for linting purpose*)

## Running/Usage

```bash
Usage:
  make <target>

Targets:
  hello                 hello
  build                 build binary
  build-container       build container
  run-dev               running dev mode
  lint-go               Linter
```

### Running Redis Locally

Install redis in your local development host or can be simple with docker:

```bash
docker-compose up redis
```